import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import * as firebase from 'firebase';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { UserService } from './providers/api/user.service';
import { AuthService } from './providers/auth.service';
import { UploadFileService } from './providers/api/upload-file.service';
import { ModalService } from './providers/modal/modal.service';
import { StoreProductsComponent } from './components/customer/store-products/store-products.component';
import { HeaderComponent } from './components/customer/header/header.component';
import { ProductComponent } from './components/customer/product/product.component';

firebase.initializeApp(environment.firebase);

@NgModule({
    declarations: [
        AppComponent,
        StoreProductsComponent,
        HeaderComponent,
        ProductComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        AngularFireDatabaseModule,
    ],
    providers: [
        AuthService,
        UserService,
        UploadFileService,
        ModalService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
