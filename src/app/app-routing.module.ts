import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreProductsComponent } from './components/customer/store-products/store-products.component';
import { ProductComponent } from './components/customer/product/product.component';

const routes: Routes = [
    {
        path: 'store',
        component: StoreProductsComponent
    },
    {
        path: 'store/product/:id',
        component: ProductComponent
    },
    { path: '**', pathMatch: 'full', redirectTo: 'store'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
