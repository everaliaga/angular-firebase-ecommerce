import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-store-products',
    templateUrl: './store-products.component.html',
    styleUrls: ['./store-products.component.css']
})
export class StoreProductsComponent implements OnInit {

    focusPriceInProductId: string;
    focusPhotoInProductId: string;

    constructor(private router: Router) { }

    ngOnInit() {
    }

    viewDetailProduct(item: string) {
        this.router.navigate(['store/product/', item]);
    }

}
