export class Store {
    key: string;
    name: string;
    photoURL: string;
    description: string;
    address: string;

    constructor() {
    }
}
