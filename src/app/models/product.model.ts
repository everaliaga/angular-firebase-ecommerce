export class Product {
    key: string;
    imageURL: string;
    name: string;
    description: string;
    price: number;

    constructor() {
    }
}
