import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { FileUpload } from '../../models/file-upload.model';
import * as firebase from 'firebase';

@Injectable()
export class UploadFileService {

    private basePath = '/uploads';

    constructor(private db: AngularFireDatabase) { }

    pushFileToStorage(fileUpload: FileUpload, progress: { percentage: number }) {
        const promise = new Promise((resolve, reject) => {
            const storageRef = firebase.storage().ref();
            const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);

            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    // in progress
                    const snap = snapshot as firebase.storage.UploadTaskSnapshot;
                    progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);
                },
                (error) => {
                    // fail
                    console.log(error);
                    reject(error);
                },
                () => {
                    // success
                    uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
                        console.log('File available at', downloadURL);
                        fileUpload.url = downloadURL;
                        fileUpload.name = fileUpload.file.name;
                        const saved = this.saveFileData(fileUpload);
                        resolve(saved.key);
                    });
                }
            );
        });
        return promise;
    }

    private saveFileData(fileUpload: FileUpload) {
        return this.db.list(`${this.basePath}/`).push(fileUpload);
    }

    getFileUploadById(fileId): AngularFireObject<FileUpload> {
        return this.db.object(`${this.basePath}/${fileId}`);
    }

    getFileUploads(numberItems): AngularFireList<FileUpload> {
        return this.db.list(this.basePath, ref =>
            ref.limitToLast(numberItems));
    }

    deleteFileUpload(fileUpload: FileUpload) {
        this.deleteFileDatabase(fileUpload.key)
            .then(() => {
                this.deleteFileStorage(fileUpload.name);
            })
            .catch(error => console.log(error));
    }

    private deleteFileDatabase(key: string) {
        return this.db.list(`${this.basePath}/`).remove(key);
    }

    private deleteFileStorage(name: string) {
        const storageRef = firebase.storage().ref();
        storageRef.child(`${this.basePath}/${name}`).delete();
    }

}
