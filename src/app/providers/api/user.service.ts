﻿import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { User } from 'src/app/models/user.model';

@Injectable()
export class UserService {

    private basePath = '/users';

    restaurantsRef: AngularFireList<User> = null;

    constructor(private db: AngularFireDatabase) {
        this.restaurantsRef = db.list(this.basePath);
    }

    getUserById(userId: string): AngularFireObject<User> {
        return this.db.object(`${this.basePath}/${userId}`);
    }

}
