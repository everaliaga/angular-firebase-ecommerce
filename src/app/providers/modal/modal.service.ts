import { Injectable, ComponentFactoryResolver, ApplicationRef, Injector, EmbeddedViewRef, ComponentRef } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ModalService {

    componentRef: ComponentRef<any>;

    constructor(private componentFactoryResolver: ComponentFactoryResolver,
        private appRef: ApplicationRef,
        private injector: Injector
    ) {

    }

    open(component: any) {
        if (!this.componentRef) {
            this.componentRef = this.componentFactoryResolver.resolveComponentFactory(component).create(this.injector);

            setTimeout(() => {
                this.appRef.attachView(this.componentRef.hostView);
                const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
                document.body.appendChild(domElem);
            }, 1);
        }
    }

    close() {
        this.appRef.detachView(this.componentRef.hostView);
        this.componentRef.destroy();
        this.componentRef = null;
    }
}
