import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserService } from './api/user.service';

interface User {
    uid: string;
    email?: string | null;
    phoneNumber?: string | null;
    photoURL?: string;
    displayName?: string;
}

@Injectable()
export class AuthService {

    user: Observable<User | null>;

    constructor(public afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private router: Router,
        private userService: UserService
    ) {

    }


    login(email, password) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.auth.signInWithEmailAndPassword(email, password).then(
                res => {
                    console.log(res);
                    this.userService.getUserById(res.user.uid).valueChanges()
                        .subscribe(result => {
                            localStorage.setItem('afe-userId', res.user.uid);
                            localStorage.setItem('afe-role', result.role);
                            resolve(res);
                        });
                },
                err => {
                    console.log(err);
                    reject(err);
                }
            );
        });
    }

    doLogout() {
        this.afAuth.auth.signOut().then(() => {
            localStorage.removeItem('afe-userId');
            localStorage.removeItem('afe-role');
            this.router.navigate(['/login']);
        });
    }

    isAuthenticated(): boolean {
        const user = localStorage.getItem('afe-userId');
        if (user) {
            return true;
        }
        else {
            return false;
        }
    }

    getUserRole() {
        return localStorage.getItem('afe-role');
    }

    private updateUserData(user: User) {
        localStorage.setItem('user', JSON.stringify(user));
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(
            `users/${user.uid}`
        );

        const data: User = {
            uid: user.uid,
            email: user.email || null,
            phoneNumber: user.phoneNumber || null,
            displayName: user.displayName || 'nameless user',
            photoURL: user.photoURL || 'https://goo.gl/Fz9nrQ'
        };
        return userRef.set(data);
    }

    doFacebookLogin() {
        return new Promise<any>((resolve, reject) => {
            const provider = new firebase.auth.FacebookAuthProvider();
            this.afAuth.auth.signInWithPopup(provider).then(
                res => {
                    this.updateUserData(res.user);
                    resolve(res);
                },
                err => {
                    console.log(err);
                    reject(err);
                }
            );
        });
    }

    doGoogleLogin() {
        return new Promise<any>((resolve, reject) => {
            const provider = new firebase.auth.GoogleAuthProvider();
            provider.addScope('profile');
            provider.addScope('email');
            this.afAuth.auth.signInWithPopup(provider).then(
                res => {
                    console.log(res);

                    this.updateUserData(res.user);
                    resolve(res);
                },
                err => {
                    console.log(err);
                    reject(err);
                }
            );
        });
    }

}
